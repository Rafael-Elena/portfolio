const Angularproject = require ('../models/Angularproject');

const getAllAngularproject = async ( req, res, next) => {
    try {
    const allAngularproject = await Angularproject.find();
    res.status(200).json ({
        data: allAngularproject,
    });
    } catch (err) {
        res.status(500).json ({
            data: 'Could not charge Angular Project'
        });
    }
};
const postAllAngularproject = async (req, res, next) => {
    try {
    const { name, type, description, urlbehance } = req.body;

    const newAngularproject= new Angularproject({
        name,
        type,
        description,
        urlbehance,
    });

    await newAngularproject.save()

    res.status(201).json ({
        data: newAngularproject,
    });
} catch (err) {
    res.status(500).json ({
        data: 'Could not create Angular Project'
    });
}
}

module.exports = {
    getAllAngularproject,
    postAllAngularproject,
}
