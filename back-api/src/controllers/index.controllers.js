const { welcomeMail, sendContactEmail } = require('../config/nodemailer');

const getHealthCheck  = (req, res, next) => {
    res.status(200).json({
        data: 'Hi there! I´m alive, HealthCheck done!',
    });
};


const postEmail = async (req, res, next) => {
    const { recipient } = req.body;
    await welcomeMail(recipient);

    res.status(200).json({ data: 'Email sent!'});
}

const postSendEmail = async (req, res, next) => {
    const { sender, title, message } = req.body;
    await sendContactEmail(sender, title, message );

    res.status(200).json({ data: 'Email sent!'});
}

module.exports = {
    getHealthCheck,
    postEmail,
    postSendEmail,
}