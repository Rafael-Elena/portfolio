const Landing = require ('../models/Landing');

const getAllLanding = async ( req, res, next) => {
    try {
    const allLanding = await Landing.find();
    res.status(200).json ({
        data: allLanding,
    });
    } catch (err) {
        res.status(500).json ({
            data: 'Could not charge Landing'
        });
    }
};

const postAllLanding = async (req, res, next) => {
    try {
    const { work, description, about, savia, royal, esports, angular, react, letstalk, letstalkdescription, name, me } = req.body;

    const newLanding = new Landing({
        work,
        description,
        about, savia,
        royal,
        esports,
        angular,
        react,
        letstalk,
        letstalkdescription,
        name,
        me,
    });

    await newLanding.save()

    res.status(201).json ({
        data: newLanding,
    });
} catch (err) {
    res.status(500).json ({
        data: 'Could not create Landing'
    });
}
}
module.exports =  {
    getAllLanding,
    postAllLanding,
};