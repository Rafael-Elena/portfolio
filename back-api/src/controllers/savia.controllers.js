const Savia = require('../models/Savia')

const getAllSavia = async ( req, res, next) => {
    try {
    const allSavia = await Savia.find();
    res.status(200).json ({
        data: allSavia,
    });
    } catch (err) {
        res.status(500).json ({
            data: 'Could not charge Savia'
        });
    }
};

const postAllSavia = async (req, res, next) => {
    try {
    const { name, type, description, urlbehance, web  } = req.body;

    const newSavia = new Savia({
        name,
        type,
        description,
        urlbehance,
        web
    });

    await newSavia.save()

    res.status(201).json ({
        data: newSavia,
    });
} catch (err) {
    res.status(500).json ({
        data: 'Could not create Savia'
    });
}
}

module.exports = {
    getAllSavia,
    postAllSavia,
};
