const { Router } = require('express');

const { getAllSavia, postAllSavia } = require('../controllers/savia.controllers');

const router = Router();

router.get('/', getAllSavia);

router.post('/', postAllSavia)

module.exports = router;