const { Router } = require('express');

const { getAllLanding, postAllLanding } = require('../controllers/landing.controllers');

const router = Router();

router.get('/', getAllLanding);

router.post('/', postAllLanding);

module.exports = router;