const { Router } = require('express');

const { getHealthCheck } = require ('../controllers/index.controllers');

const { postEmail, postSendEmail } = require ('../controllers/index.controllers');

const router = Router();

router.get('/', getHealthCheck);

router.post('/letstalk', postEmail);
router.post('/contact', postSendEmail)

module.exports = router;