const { Router } = require('express');

const { getAllAngularproject, postAllAngularproject } = require('../controllers/angularproject.controllers');

const router = Router();

router.get('/', getAllAngularproject);

router.post('/', postAllAngularproject);

module.exports = router;