const nodemailer = require('nodemailer');

const EMAIL = process.env.EMAIL;
const PASSWORD = process.env.PASSWORD;

const transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: EMAIL,
        pass: PASSWORD,
    }
});


const welcomeMail = async (recipient) => {
    try {
    const info = await transporter.sendMail({
        from: `"Rafael Elena" <${EMAIL}>`,
        to: recipient,
        subject: 'Thank you! I´ll contact you asap ☄️',
        html: `
        <p>Email sent, thanks!</p>
        <p>Rafa</p>
        `
    })
    } catch (err) {
        console.error('Can´t send this email:');
        console.error(err);
    }
};

const sendContactEmail = async (sender, message, title) => {
    try {
        await transporter.sendMail({
            from: `"Customer" <${EMAIL}>`,
            to: EMAIL,
            subject: `Customer Request: ${title} ☄️`,
            html: `
            <p>Customer ${sender} said: </p>
            <p>${message}</p>
            `
        })
        } catch (err) {
            console.error('Can´t send this email:');
            console.error(err);
        }
}

module.exports = {
    welcomeMail,
    sendContactEmail,
};