const mongoose = require('mongoose')

const DB_URI = 'mongodb://localhost:27017/portfoliorafaelelena'

mongoose
    .connect(DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(() => {
        console.log(`Connected to db: ${DB_URI}`);
    })
    .catch((err) => {
        console.log('There was an error connecting to DB');
        console.log(err);
    });

