const  mongoose  = require("mongoose")

const saviaSchema = new mongoose.Schema({
    name : { type: String, required: true },
    type : { type: String, required: true },
    description : { type: String, required: true },
    urlbehance : { type: String, required: true },
    web : { type: String, required: true },
});


const Savia = mongoose.model('Savia', saviaSchema);
module.exports = Savia;