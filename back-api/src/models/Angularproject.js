const  mongoose  = require("mongoose")

const angularprojectSchema = new mongoose.Schema({
    name : { type: String, required: true },
    type : { type: String, required: true },
    description : { type: String, required: true },
    urlbehance : { type: String, required: true },
});


const Angularproject = mongoose.model('Angularproject', angularprojectSchema);
module.exports = Angularproject;