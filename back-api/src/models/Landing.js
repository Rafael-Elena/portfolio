const  mongoose  = require("mongoose")

const landingSchema = new mongoose.Schema({
    work : { type: String, required: true },
    description : { type: String, required: true },
    about : { type: String, required: true },
    savia : { type: String, required: true },
    royal : { type: String, required: true },
    esports : { type: String, required: true },
    angular: { type: String, required: true },
    react : { type: String, required: true },
    letstalk : { type: String, required: true },
    letstalkdescription : { type: String, required: true },
    name : { type: String, required: true },
    me: { type: String, required: true },
});


const Landing = mongoose.model('Landing', landingSchema);
module.exports = Landing;