require('dotenv').config();

const express = require('express');
const cors = require('cors');

require('./config/database');

const indexRouter = require('./routes/index.routes');
const landingRouter = require('./routes/landing.routes');
const saviaRouter = require('./routes/savia.routes');
const angularprojectRouter = require('./routes/angularproject.routes');
const letstalkRouter = require('./routes/index.routes');


const app = express();


app.use(cors());
//Esto tranforma los body a Json cuando llegan a POST/PUT
app.use(express.json());


app.use('/', indexRouter);
// Hago request a localhost//4000/landing
app.use('/landing', landingRouter);
// Hago request a localhost//4000/savia
app.use('/savia', saviaRouter);

app.use('/angularproject', angularprojectRouter);

app.use('/contact', letstalkRouter);

const PORT = 4000;
app.listen(PORT, () => {
    console.log(`Listen in PORT ${PORT}`);
});