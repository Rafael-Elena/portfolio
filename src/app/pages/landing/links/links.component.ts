import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-links',
  templateUrl: './links.component.html',
  styleUrls: ['./links.component.scss']
})
export class LinksComponent implements OnInit {
  public status: boolean = false;

  constructor() { }

  ngOnInit(): void {
    this.clickEvent();
  }


  clickEvent(){
    this.status = !this.status;
}

}
