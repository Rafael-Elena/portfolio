import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Landing } from '../../models/Ilanding';

@Injectable({
  providedIn: 'root'
})
export class LandingService {
  private landingUrl = 'http://localhost:4000/landing';

  constructor(private httpClient: HttpClient) { }

  public getLanding(): Observable<any> {
    return this.httpClient.get(this.landingUrl).pipe(map((response: Landing) => {
      if (!response) {
        throw new Error('Value Expected');
      } else {
        return response;
      }
    }),
    catchError(error => {
      throw new Error(error.message);
    }));
  }
}
