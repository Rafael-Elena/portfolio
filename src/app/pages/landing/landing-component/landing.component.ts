import { LandingService } from './service/landing.service';
import { Component, OnInit } from '@angular/core';
import { Landing } from '../models/Ilanding';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public title: string;
  public landingInfo: Landing;
  public status: boolean = false;

  constructor(public landingService: LandingService, ) {
    this.title = 'Landing';
  }

  ngOnInit(): void {
    this.getLandingData();
    this.clickEvent();
  }


  public getLandingData(): void {
    this.landingService.getLanding().subscribe(({ data }) => {
      this.landingInfo = data;
    }, (error) => {
      console.error(error.message);
    });
  }

  clickEvent(){
      this.status = !this.status;
  }
}