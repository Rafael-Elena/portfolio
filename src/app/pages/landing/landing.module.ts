import { NgxLottieViewModule } from 'ngx-lottie-view';
import { LottieAnimationViewModule } from 'ng-lottie';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { LandingComponent } from './landing-component/landing.component';
import { LinksComponent } from './links/links.component';


@NgModule({
  declarations: [LandingComponent, LinksComponent],
  imports: [
    CommonModule,
    LandingRoutingModule,
    LottieAnimationViewModule.forRoot(),
    NgxLottieViewModule,

  ]
})
export class LandingModule { }


