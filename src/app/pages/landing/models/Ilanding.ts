export interface  Landing {
    work: string;
    description: string;
    about: string;
    savia: string;
    royal: string;
    esports: string;
    angular: string;
    react: string;
    letstalk: string;
    letstalkdescription: string;
    name: string;
    me: string;

}