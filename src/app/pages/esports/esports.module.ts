import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EsportsRoutingModule } from './esports-routing.module';
import { EsportsComponent } from './esports-component/esports.component';

@NgModule({
  declarations: [EsportsComponent],
  imports: [
    CommonModule,
    EsportsRoutingModule,
  ]
})
export class EsportsModule { }
