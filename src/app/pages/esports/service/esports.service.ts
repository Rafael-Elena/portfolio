import { Esports } from '../models/Iesports';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class EsportsService {
  private esportsUrl = 'http://localhost:3000/esports';

  constructor(private httpClient: HttpClient) {
  }
  public getEsports(): Observable<any> {
    return this.httpClient.get(this.esportsUrl).pipe(map((response: Esports) => {
      if (!response) {
        throw new Error('Value Expected');
      } else {
        return response;
      }
    }),
    catchError(error => {
      throw new Error(error.message);
    }));
  }
}
