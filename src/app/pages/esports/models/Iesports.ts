export interface Esports {
    img: string;
    name: string;
    type: string;
    description: string;
    urlbehance: string;
}