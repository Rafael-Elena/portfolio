import { EsportsService } from './../service/esports.service';
import { Component, OnInit } from '@angular/core';
import { Esports } from '../models/Iesports';



@Component({
  selector: 'app-esports',
  templateUrl: './esports.component.html',
  styleUrls: ['./esports.component.scss']
})
export class EsportsComponent implements OnInit {
  public title: string;
  public esportsInfo: Esports;


  constructor(public esportsService: EsportsService) {
    this.title = 'Esports';
  }

  ngOnInit(): void {
    this.getEsportsData();

  }


  public getEsportsData(): void {
    this.esportsService.getEsports().subscribe((data: Esports) => {
      this.esportsInfo = data;
    }, (error) => {
      console.error(error.message);
    });
  }

}
