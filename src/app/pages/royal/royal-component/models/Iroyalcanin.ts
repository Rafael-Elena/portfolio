export interface Royal {
    img: string;
    name: string;
    type: string;
}