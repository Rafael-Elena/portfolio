import { Royal } from './models/Iroyalcanin';
import { RoyalService } from './../service/royal.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-royal',
  templateUrl: './royal.component.html',
  styleUrls: ['./royal.component.scss']
})
export class RoyalComponent implements OnInit {
  public title: string;
  public royalInfo: Royal;

  constructor(public royalService: RoyalService) {
    this.title = 'Royal';
  }

  ngOnInit(): void {
    this.getRoyalData();
  }

  public getRoyalData(): void {
    this.royalService.getRoyal().subscribe((data: Royal) => {
      this.royalInfo = data;
    }, (error) => {
      console.error(error.message);
    });
  }

}
