import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoyalRoutingModule } from './royal-routing.module';
import { RoyalComponent } from './royal-component/royal.component';


@NgModule({
  declarations: [RoyalComponent],
  imports: [
    CommonModule,
    RoyalRoutingModule
  ]
})
export class RoyalModule { }
