import { TestBed } from '@angular/core/testing';

import { RoyalService } from './royal.service';

describe('RoyalService', () => {
  let service: RoyalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoyalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
