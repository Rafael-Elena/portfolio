import { Royal } from '../royal-component/models/Iroyalcanin';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoyalService {
  private royalUrl = 'http://localhost:3000/royal';

  constructor(private httpClient: HttpClient) {
  }
  public getRoyal(): Observable<any> {
    return this.httpClient.get(this.royalUrl).pipe(map((response: Royal) => {
      if (!response) {
        throw new Error('Value Expected');
      } else {
        return response;
      }
    }),
    catchError(error => {
      throw new Error(error.message);
    }));
  }
}

