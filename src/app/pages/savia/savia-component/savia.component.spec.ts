import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaviaComponent } from './savia.component';

describe('SaviaComponent', () => {
  let component: SaviaComponent;
  let fixture: ComponentFixture<SaviaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaviaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaviaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
