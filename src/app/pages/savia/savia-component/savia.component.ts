import { SaviaService } from '../service/savia.service';
import { Component, OnInit } from '@angular/core';
import { Savia } from 'src/app/pages/savia/models/Isavia';

@Component({
  selector: 'app-savia',
  templateUrl: './savia.component.html',
  styleUrls: ['./savia.component.scss']
})
export class SaviaComponent implements OnInit {
  public title: string;
  public saviaInfo: Savia;

  constructor(public saviaService: SaviaService) {
    this.title = 'Savia';
  }

  ngOnInit(): void {
    this.getSaviaData();
  }


  public getSaviaData(): void {
    this.saviaService.getSavia().subscribe(({data}) => {
      this.saviaInfo = data;
    }, (error) => {
      console.error(error.message);
    });
  }

}
