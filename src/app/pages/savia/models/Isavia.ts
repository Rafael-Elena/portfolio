export interface Savia {
    name: string;
    type: string;
    description: string;
    urlbehance: string;
    web: string;
}
