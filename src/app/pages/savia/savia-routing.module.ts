import { LottieAnimationViewModule } from 'ng-lottie';
import { NgxLottieViewModule } from 'ngx-lottie-view';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaviaComponent } from './savia-component/savia.component';

const routes: Routes = [{
  path: '',
  component: SaviaComponent,
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    LottieAnimationViewModule.forRoot(),
    NgxLottieViewModule
  ],
  exports: [RouterModule]
})
export class SaviaRoutingModule { }
