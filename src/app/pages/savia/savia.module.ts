import { LottieAnimationViewModule } from 'ng-lottie';
import { NgxLottieViewModule } from 'ngx-lottie-view';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaviaRoutingModule } from './savia-routing.module';
import { SaviaComponent } from './savia-component/savia.component';



@NgModule({
  declarations: [SaviaComponent],
  imports: [
    CommonModule,
    SaviaRoutingModule,
    LottieAnimationViewModule.forRoot(),
    NgxLottieViewModule
  ]
})
export class SaviaModule { }
