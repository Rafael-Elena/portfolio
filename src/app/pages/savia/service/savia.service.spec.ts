import { TestBed } from '@angular/core/testing';

import { SaviaService } from './savia.service';

describe('SaviaService', () => {
  let service: SaviaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SaviaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
