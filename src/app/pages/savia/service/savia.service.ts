import { Savia } from './../models/Isavia';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SaviaService {
  private saviaUrl = 'http://localhost:4000/savia';

  constructor(private httpClient: HttpClient) {
  }
  public getSavia(): Observable<any> {
    return this.httpClient.get(this.saviaUrl).pipe(map((response: Savia) => {
      if (!response) {
        throw new Error('Value Expected');
      } else {
        return response;
      }
    }),
    catchError(error => {
      throw new Error(error.message);
    }));
  }

}
