import { Letstalk, Contact } from './../models/Iletstalk';
import { LetstalkService } from './../service/letstalk.service';
import { Component, OnInit } from '@angular/core';



  @Component({
  selector: 'app-letstalk',
  templateUrl: './letstalk.component.html',
  styleUrls: ['./letstalk.component.scss']
})
export class LetstalkComponent implements OnInit {
  public title: string;
  public letstalkInfo: Letstalk;
  public contactEmail: Contact;



  constructor (
    public letstalkService: LetstalkService, ) {}


  ngOnInit(): void {
    this.getTalk();

  }


  public getTalk(): void {
    this.letstalkService.getTalk().subscribe((data: Letstalk) => {
      this.letstalkInfo = data;
    }, (error) => {
      console.error(error.message);
    });
  }


}