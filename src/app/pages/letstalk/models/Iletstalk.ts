export interface Letstalk {
    description: string;
    letstalk: string;

}

export interface Contact {
    sender: string;
    title: string;
    message: string;

}