import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxLottieViewModule } from 'ngx-lottie-view';
import { LottieAnimationViewModule } from 'ng-lottie';


import { LetstalkRoutingModule } from './letstalk-routing.module';
import { LetstalkComponent } from './letstalk-component/letstalk.component';

@NgModule({
  declarations: [LetstalkComponent],
  imports: [
    CommonModule,
    LetstalkRoutingModule,
    LottieAnimationViewModule.forRoot(),
    NgxLottieViewModule,

  ]
})
export class LetstalkModule { }
