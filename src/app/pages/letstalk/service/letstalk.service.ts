import { Letstalk } from './../models/Iletstalk';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LetstalkService {
  private letstalkUrl = 'http://localhost:4000/contact'
  constructor(private httpClient: HttpClient) { }
  public getTalk(): Observable<any> {
    return this.httpClient.get(this.letstalkUrl).pipe(map((response: Letstalk) => {
      if (!response) {
        throw new Error('Value Expected');
      } else {
        return response;
      }
    }),
    catchError(error => {
      throw new Error(error.message);
    }));
  }
}


