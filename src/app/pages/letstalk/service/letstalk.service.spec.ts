import { TestBed } from '@angular/core/testing';

import { LetstalkService } from './letstalk.service';

describe('LetstalkService', () => {
  let service: LetstalkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LetstalkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
