import { LottieAnimationViewModule } from 'ng-lottie';
import { NgxLottieViewModule } from 'ngx-lottie-view';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComunicationService } from 'src/services/comunication.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AnimateOnScrollModule } from 'ng2-animate-on-scroll';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LottieAnimationViewModule.forRoot(),
    NgxLottieViewModule,
    ReactiveFormsModule,
    AnimateOnScrollModule.forRoot(),
  ],
  providers: [ComunicationService],
  bootstrap: [AppComponent]
})

export class AppModule { }
