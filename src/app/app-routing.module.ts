import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
{
  path: 'landing',
  loadChildren: () => import('./pages/landing/landing.module').then((m) => m.LandingModule)
},
{
  path: 'esports',
  loadChildren: () => import('./pages/esports/esports.module').then((m) => m.EsportsModule)
},
{
  path: 'savia',
  loadChildren: () => import('./pages/savia/savia.module').then((m) => m.SaviaModule)
},
{
  path: 'royal',
  loadChildren: () => import('./pages/royal/royal.module').then((m) => m.RoyalModule)
},
{
  path: 'angular',
  loadChildren: () => import('./pages/angular/angular.module').then((m) => m.AngularModule)
},
{
  path: 'react',
  loadChildren: () => import('./pages/react/react.module').then((m) => m.ReactModule)
},
{
  path: 'letstalk',
  loadChildren: () => import('./pages/letstalk/letstalk.module').then((m) => m.LetstalkModule)
},
{
  path: '',
  redirectTo: 'landing',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
