export const ENDPOINTS = {
    esports: 'http://localhost:4200/esports',
    royal: 'http://localhost:4200/royal',
    savia: 'http://localhost:4200/savia',
    landing: 'http://localhost:4200/landing',
    letstalk: 'http://localhost:4200/letstalk',
}